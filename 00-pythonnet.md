## Problem statement:
- I want to use pythonnet - Python.NET

## Resource:
- https://github.com/pythonnet/pythonnet

## Installing manually (as of 2021/09/23)
- pypi package v2.5.2 conda-forge v2.5.2 python 2.7, 3.5, 3.6, 3.8 (missing 3.9)
- conda is python-3.9 based
- using miniconda3 as starting point (smaller)

## Assumptions:
- linux based environment on x86_64 compatible cpu.
- user can install software in $HOME or in some other storage space.

## Installation in $HOME/miniconda3 and updating it
```
curl -qsSLkO https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
&& bash Miniconda3-latest-Linux-x86_64.sh -b -p $HOME/miniconda3 \
&& rm Miniconda3-latest-Linux-x86_64.sh
$HOME/miniconda3/bin/conda update conda && $HOME/miniconda3/bin/conda update --all
```

note, ymmv: if your $HOME is too small (as for the 10GB limit on maestro), you might 
want to replace $HOME by another path where you have more disk quota.

## Adding pythonnet in its own environment
```
conda create -n pythonnet
conda activate pythonnet
conda install -c conda-forge pythonnet
```

## Testing
Assuming you have already activated your pythonnet environment:
```
python -c 'import clr'
```
## Exporting the recipe: (I have a folder for that purpose in $HOME/conda.d)
```
conda env export --name pythonnet > ~/conda.d/pythonnet.yml
```

From your activated environment you can also experiment with:
```
conda env export                > conda-env-export.yml
conda env export --from-history > conda-env-export-from-history.yml
conda env export --no-builds    > conda-env-export-no-builds.yml
```

## Re-using the recipe (yml file)
```
conda env create -n pythonnet-from-yml --file ~/conda.d/pythonnet.yml
```

### Using `conda list --explicit` instead of `conda env export`
```
conda activate pythonnet
conda list --explicit > ~/conda.d/pythonnet-explicit.yml
```

re-use with:
```
conda create --name toto --file ~/conda.d/pythonnet-explicit.yml
```

## Adding the yml file in the projet
Just upload the file to your gitlab to make it available later from
your container builder or colleague.

From maestro if you have configured fex (http://dl.pasteur.fr) you can
upload it for 30 days.
```
(pythonnet) [tru@maestro-submit conda.d]$ fexsend pythonnet.yml .
pythonnet.yml: 1 kB in 1 s = 1 kB/s
Recipient: tru@pasteur.fr (autodelete=NO,keep=40,locale=english,notification=full)
Location: http://dl.pasteur.fr/fop/bD6Bmqpc/pythonnet.yml
```

## Caveats
- I prefer to only activate conda as needed, as python can be used by
your linux distribution and conda version might interfere in unexpected ways.
I prefer to remove the conda initialisation lines that have been added in
your .bashrc file and only source there lines when required.
Do this once:
```
CONDA_ROOT=$HOME/miniconda3
export CONDA_ROOT
${CONDA_ROOT}/bin/conda shell.bash hook > $HOME/.miniconda.bashrc
```
When you need conda:
```
source  $HOME/.miniconda.bashrc
```
- do not use ~/.local python (as all python versions will use it)
```
PYTHONNOUSERSITE=1 
export PYTHONNOUSERSITE
```

