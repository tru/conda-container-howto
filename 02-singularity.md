## Problem statement:
- building a singularity container from the conda yml file.

## Why
- conda environment are providing some lever of isolation but you may break it later by adding packages through conda or pip 
- singularity container: building in a sandbox to avoid messing with your environments
- easily discared and re-used.
- singularity is more readily available on shared resources than docker.

## Assumptions:
- you have singularity installed on your machine an you can run it as root to build from a recipe,
- privilege build are seldom the case of most of the HPC center, it is not available on maestro but it is available on the Inception gpulab
- alternatively, you can use singularity to build from a docker registry without root privilege (on maestro): that is the teaser for the gitlab CI

## Installation in /opt/miniconda3, updating it, adding pythonnet

Singularity file (looks very much like the Dockerfile...)
```
Bootstrap: docker
From: centos:centos7

%post
yum -y update && \
yum -y install bzip2 && \
yum -y clean all

# install miniconda in /opt/miniconda, updating and adding pythonnet
curl -qsSLkO https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
&& bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/miniconda3 \
&& rm Miniconda3-latest-Linux-x86_64.sh
/opt/miniconda3/bin/conda update conda && /opt/miniconda3/bin/conda update --all
/opt/miniconda3/bin/conda install -c conda-forge pythonnet
#
%environment
PATH=/opt/miniconda3/bin:$PATH
export PATH
# do not use ~/.local python
PYTHONNOUSERSITE=1 
export PYTHONNOUSERSITE
```
## Installation in /opt/miniconda3, updating it, adding though a yml file

Singularity file (looks very much like the Dockerfile...)
```
Bootstrap: docker
From: centos:centos7

%post
yum -y update && \
yum -y install bzip2 && \
yum -y clean all

# install miniconda in /opt/miniconda, updating and adding pythonnet
curl -qsSLkO https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
&& bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/miniconda3 \
&& rm Miniconda3-latest-Linux-x86_64.sh
/opt/miniconda3/bin/conda update conda && /opt/miniconda3/bin/conda update --all
# download the yml file
curl  https://gitlab.pasteur.fr/tru/conda-container-howto/-/raw/main/pythonnet.yml > pythonnet.yml && \
/opt/miniconda3/bin/conda env create --file pythonnet.yml

%environment
PATH=/opt/miniconda3/bin:$PATH
export PATH
# do not use ~/.local python
PYTHONNOUSERSITE=1 
export PYTHONNOUSERSITE

#
```
## You can also choose to use the miniconda image from Anaconda:

Singularity file:
```
Bootstrap: docker
From: continuumio/miniconda3

%post
# conda lives under /opt/conda
conda update conda && conda update --all -y
conda install -c conda-forge pythonnet

%environment
PATH=/opt/conda/bin:$PATH
export PATH
# do not use ~/.local python
PYTHONNOUSERSITE=1 
export PYTHONNOUSERSITE
```

Singularity file:
```
Bootstrap: docker
From: continuumio/miniconda3

%post
# conda lives under /opt/conda
conda update conda && conda update --all -y

# download the yml file there is no curl, only wget by default
wget https://gitlab.pasteur.fr/tru/conda-container-howto/-/raw/main/pythonnet.yml  && \
conda env create --file pythonnet.yml

%environment
PATH=/opt/conda/bin:$PATH
export PATH
# do not use ~/.local python
PYTHONNOUSERSITE=1 
export PYTHONNOUSERSITE
```

## Full example output on the gpulab
```
[tru@adm pythonnet]$ wget https://gitlab.pasteur.fr/tru/conda-container-howto/-/
raw/main/Singularity
--2021-09-23 14:06:00--  https://gitlab.pasteur.fr/tru/conda-container-howto/-/raw/main/Singularity
Resolving gitlab.pasteur.fr (gitlab.pasteur.fr)... 157.99.21.44
Connecting to gitlab.pasteur.fr (gitlab.pasteur.fr)|157.99.21.44|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 769 [text/plain]
Saving to: ‘Singularity’

100%[======================================>] 769         --.-K/s   in 0s

2021-09-23 14:06:00 (115 MB/s) - ‘Singularity’ saved [769/769]

[tru@adm pythonnet]$ sudo singularity build b.sif Singularity                   INFO:    Starting build...
Getting image source signatures
Copying blob 2d473b07cdd5 [--------------------------------------] 0.0b / 0.0b
Copying config 6d42e44a9f done
Writing manifest to image destination
<...>
#
# To activate this environment, use
#
#     $ conda activate pythonnet
#
# To deactivate an active environment, use
#
#     $ conda deactivate

INFO:    Adding environment to container
INFO:    Creating SIF file...
INFO:    Build complete: b.sif
[tru@adm pythonnet]$ ./b.sif 
Singularity> eval "$(conda shell.bash hook)"
(base) Singularity> conda activate pythonnet
(pythonnet) Singularity> python -c 'import clr'
(pythonnet) Singularity> 
```
Now you just need to distribute your container... to whoever/wherever.

## Caveats
- adding to base and adding a new pythonnet env are different things...
- make sure you knwo where pythonnet is install and activate (or not) the proper env.
your linux distribution and conda version might interfere in unexpected ways.
- do not use ~/.local python (as all python versions will use it)
```
PYTHONNOUSERSITE=1 
export PYTHONNOUSERSITE
```

