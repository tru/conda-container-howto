From centos:centos7
# update your OS
RUN yum -y update && \
    yum -y install bzip2 && \
    yum -y clean all

# install miniconda in /opt/miniconda, updating and adding pythonnet env
RUN curl -qsSLkO https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
&& bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/miniconda3 \
&& rm Miniconda3-latest-Linux-x86_64.sh
RUN /opt/miniconda3/bin/conda update conda && /opt/miniconda3/bin/conda update --all
# download the yml file
RUN curl  https://gitlab.pasteur.fr/tru/conda-container-howto/-/raw/main/pythonnet.yml > pythonnet.yml
RUN /opt/miniconda3/bin/conda env create --file pythonnet.yml
