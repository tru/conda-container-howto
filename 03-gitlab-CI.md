## Problem statement:
- I want to use gitlab to build my docker image and use it with singularity on maestro

## Assumptions:
- singularity module on maestro (just `module load singularity`)
- gitlab docker registry which is hosting the image built from the Dockerfile

## Howto
- create/copy/adapt the .gitlab-ci.yml file (once)
.gitlab-ci.yml
```
image: registry-gitlab.pasteur.fr/dsi-tools/docker-images/docker:latest

services:
    - registry-gitlab.pasteur.fr/dsi-tools/docker-images/docker:dind
stages:
    - build

build:
  stage: build
  script:
    - docker login -u gitlab-ci-token -p "$CI_BUILD_TOKEN" registry-gitlab.pasteur.fr
    - docker build --pull -f Dockerfile -t "$CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME"
    - docker tag "$CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME" "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - docker tag  "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"  "$CI_REGISTRY_IMAGE:latest"
    - docker push "$CI_REGISTRY_IMAGE:latest"
    - export   MY_TIME=`date +"%F-%H%M"`
    - docker tag  "$CI_REGISTRY_IMAGE:latest" "$CI_REGISTRY_IMAGE:$MY_TIME" 
    - docker push "$CI_REGISTRY_IMAGE:$MY_TIME"
```
- every push will rebuid the docker image (check the jobs at https://gitlab.pasteur.fr/tru/conda-container-howto/-/jobs)
- tags added: latest, YYYY-MM-DD-hhmm, short-hash, long-hash
- `singularity build my.sif docker://registry-gitlab.pasteur.fr/tru/conda-container-howto:latest`
- https://gitlab.pasteur.fr/tru/conda-container-howto/container_registry
  copy the tag you want (`registry-gitlab.pasteur.fr/tru/conda-container-howto:2021-09-23-1139` for instance)
```
[tru@maestro-submit ~]$ module add singularity
[tru@maestro-submit ~]$ singularity build /tmp/a.sif docker://registry-gitlab.pasteur.fr/tru/conda-container-howto:2021-09-23-1139
INFO:    Starting build...
Getting image source signatures
Copying blob 2d473b07cdd5 skipped: already exists  
Copying blob 1a591a021b37 done  
Copying blob c0c3ff4ea0d5 done  
Copying blob f475f673d1f0 done  
Copying blob 1d12568db87c done  
Copying blob 8cffb3effd95 done  
Copying config 5df04f53b9 done  
Writing manifest to image destination
Storing signatures
2021/09/23 13:54:05  info unpack layer: sha256:2d473b07cdd5f0912cd6f1a703352c82b512407db6b05b43f2553732b55df3bc
2021/09/23 13:54:05  warn rootless{usr/bin/newgidmap} ignoring (usually) harmless EPERM on setxattr "security.capability"
2021/09/23 13:54:05  warn rootless{usr/bin/newuidmap} ignoring (usually) harmless EPERM on setxattr "security.capability"
2021/09/23 13:54:05  warn rootless{usr/bin/ping} ignoring (usually) harmless EPERM on setxattr "security.capability"
2021/09/23 13:54:06  warn rootless{usr/sbin/arping} ignoring (usually) harmless EPERM on setxattr "security.capability"
2021/09/23 13:54:06  warn rootless{usr/sbin/clockdiff} ignoring (usually) harmless EPERM on setxattr "security.capability"
2021/09/23 13:54:07  info unpack layer: sha256:1a591a021b375badfbf07a5fbc46f8f3728b288d4751d6462642d8ef970617a6
2021/09/23 13:54:09  info unpack layer: sha256:c0c3ff4ea0d56b1bb372924a319e38dd84d9d67b5546d654879389695e6e9d49
2021/09/23 13:54:12  info unpack layer: sha256:f475f673d1f0e77984309c350c33441362a867c129b93a6ca20f03f7c70d8155
2021/09/23 13:54:14  info unpack layer: sha256:1d12568db87cb741f1e918e9c4ac49eaeeb3a8fc98e6c71247ba778e9d2dad3d
2021/09/23 13:54:14  info unpack layer: sha256:8cffb3effd95583812204a146f5aeaad07d3911c5e1c30a9280d182d501baff3
INFO:    Creating SIF file...
INFO:    Build complete: /tmp/a.sif
[tru@maestro-submit ~]$ /tmp/a.sif
Singularity> eval "$(/opt/miniconda3/bin/conda shell.bash hook)"
(base) Singularity> conda env list
# conda environments:
#
base                  *  /opt/miniconda3
pythonnet                /opt/miniconda3/envs/pythonnet

(base) Singularity> conda activate pythonnet
(pythonnet) Singularity> python -c 'import clr'
(pythonnet) Singularity> type python
python is hashed (/opt/miniconda3/envs/pythonnet/bin/python)
<...>
```

## Caveats
- docker registry is rate limited
- gitlab.pasteur.fr is using a proxy cache to avoid hitting the limit
- you can use the campus registry to keep a copy of your OS base image
- the default time limit is 1h, you can change at Settings -> CI/CD -> Timeout
- uploading to the campus registry takes time (10 GB/hour)
