## Problem statement:
- building a docker image from the conda yml file.

## Why
- conda environment are providing some lever of isolation but you may break it later by adding packages through conda or pip 
- docker: building in a sandbox to avoid messing with your environments
- easily discared and re-used.
- docker was a technical disruption and become a standard de facto

## Assumptions:
- you have docker installed on your machine an you can run it
- that is not the case of most of the HPC center, not available on maestro nor the Inception gpulab
- we will see in the 3rd part how to use gitlab CI to overcome the issue
- I am using a CentOS-7 based distribution, but you can use ubuntu/debian/... as long as it is
  supported by conda

## Installation in /opt/miniconda3, updating it, adding pythonnet

Dockerfile:
```
From: centos:centos7
# update your OS
RUN yum -y update && \
    yum -y install bzip2 && \
    yum -y clean all

# install miniconda in /opt/miniconda, updating and adding pythonnet to base
RUN curl -qsSLkO https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
&& bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/miniconda3 \
&& rm Miniconda3-latest-Linux-x86_64.sh
RUN /opt/miniconda3/bin/conda update conda && /opt/miniconda3/bin/conda update --all
RUN /opt/miniconda3/bin/conda install -c conda-forge pythonnet
#
```
## Installation in /opt/miniconda3, updating it, adding though a yml file

Dockerfile:
```
From: centos:centos7
# update your OS
RUN yum -y update && \
    yum -y install bzip2 && \
    yum -y clean all

# install miniconda in /opt/miniconda, updating and adding pythonnet env
RUN curl -qsSLkO https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
&& bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/miniconda3 \
&& rm Miniconda3-latest-Linux-x86_64.sh
RUN /opt/miniconda3/bin/conda update conda && /opt/miniconda3/bin/conda update --all
# download the yml file
RUN curl  https://gitlab.pasteur.fr/tru/conda-container-howto/-/raw/main/pythonnet.yml > pythonnet.yml
RUN /opt/miniconda3/bin/conda env create --file pythonnet.yml
#
```

## You can also choose to use the miniconda image from Anaconda:

Dockerfile:
```
From: continuumio/miniconda3

# conda lives under /opt/conda
RUN conda update conda && conda update --all -y
RUN conda install -c conda-forge pythonnet
```

Dockerfile:
```
From: continuumio/miniconda3

# conda lives under /opt/conda
RUN conda update conda && conda update --all -y

# download the yml file there is no curl, only wget by default
RUN wget https://gitlab.pasteur.fr/tru/conda-container-howto/-/raw/main/pythonnet.yml  && \
    conda env create --file pythonnet.yml
```

## Caveats
- adding to base and adding a new pythonnet env are different things...
- make sure you knwo where pythonnet is install and activate (or not) the proper env.
